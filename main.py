import unittest

from UnitTests.test_DeckCount import DeckCount
from UnitTests.test_DeckDrawException import DeckDrawException
from UnitTests.test_DeckDrawValue import DeckDrawValue
from UnitTests.test_DeckShuffle import DeckShuffle
from UnitTests.test_CardGetRank import CardGetRankRank
from UnitTests.test_CardGetSuite import CardGetSuit
from UnitTests.test_CardGetValue import CardGetValueSuite
from UnitTests.test_CardInitCorrectRankAndSuit import CardInitCorrectRankAndSuit
from UnitTests.test_CardInitRaiseValueError import CardInitRaiseValueError
from UnitTests.test_CardStr import CardStr

suite = unittest.TestSuite()

suite.addTest(unittest.makeSuite(DeckCount))
suite.addTest(unittest.makeSuite(DeckDrawException))
suite.addTest(unittest.makeSuite(DeckDrawValue))
suite.addTest(unittest.makeSuite(DeckShuffle))
suite.addTest(unittest.makeSuite(CardGetRankRank))
suite.addTest(unittest.makeSuite(CardGetSuit))
suite.addTest(unittest.makeSuite(CardGetValueSuite))
suite.addTest(unittest.makeSuite(CardInitCorrectRankAndSuit))
suite.addTest(unittest.makeSuite(CardInitRaiseValueError))
suite.addTest(unittest.makeSuite(CardStr))

# code to run the tests; PyCharm does this automatically
# so this code could be removed in PyCharm

runner = unittest.TextTestRunner()
res = runner.run(suite)
print(res)
print("*" * 20)

for i in res.failures: print(i[1])
