import unittest
from card import Card


class CardGetRankRank(unittest.TestCase):
    def setUp(self):
        self.Card0 = Card(4, 16)
        self.Card = Card(2, " ")
        self.Card1 = Card(3, 1)
        self.Card3 = Card(4, "p")
        self.Card4 = Card(5, None)
        self.Card5 = Card(0, "1")

    # Chee, this test will test for any wrong rank values that were incorrect
    def test_IncorrectRank(self):
        self.assertEqual(self.Card0.getRank(), 16, "incorrect rank")
        self.assertEqual(self.Card3.getRank(), -12, "incorrect rank")
        self.assertEqual(self.Card4.getRank(), 15, " incorrect rank")
        self.assertEqual(self.Card5.getRank(), -1, "incorrect rank")

    # Chee, this test will test for the range of rank
    def test_OutOfRangeRank(self):
        self.Card8 = Card(3, -200)
        self.assertLessEqual(self.Card8.getRank(), 13, "rank must be <= 13")
        self.Card5.__init__(2, 15)
        self.assertGreaterEqual(self.Card5.getSuit(), 1, "suit must be >= 1")

    # Chee, this test will test for the correct rank value
    def test_CorrectRank(self):
        self.Card6 = Card(2, 12)
        self.Card.__init__(2, 13)
        self.assertTrue(13, self.Card.getRank())
        self.Card1.__init__(2, 1)
        self.assertTrue(1, self.Card1.getRank())
        self.assertEqual(self.Card6.getRank(), 12, "Correct Rank is give")


if __name__ == '__main__':
    unittest.main()
