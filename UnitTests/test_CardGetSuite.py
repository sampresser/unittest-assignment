import unittest
from card import Card


class CardGetSuit(unittest.TestCase):
    def setUp(self):
        self.Card = Card(2, 13)
        self.Card1 = Card(3, 1)
        self.Card3 = Card(4, 12)
        self.Card4 = Card(5, 15)
        self.Card5 = Card(0, 15)

    # chee, this test is looking for the in correct suit values
    def test_IncorrectSuit(self):
        self.Card.__init__("P", 5)
        self.assertEqual(self.Card.getSuit(),  "suit rank 1-4")
        self.Card1.__init__(None, 13)
        self.assertEqual(self.Card1.getSuit(),  "suit rank 1-4")
        self.Card3.__init__(" ", 11)
        self.assertEqual(self.Card3.getSuit(), "suit rank 1-4")

    # Chee, this test is testing for the range of suit that is out of bounds
    def test_OutOfRangeSuit(self):
        self.Card4.__init__(100, 2)
        self.assertLessEqual(self.Card4.getSuit(), 4, "suit must be <= 4")
        self.Card5.__init__(-200, 12)
        self.assertGreaterEqual(self.Card5.getSuit(), 1, "suit must be >= 1")

    # chee, this test is testing for the correct suit value
    def test_CorrectSuit(self):
        self.assertEqual(self.Card.getSuit(), 2, "Correct suit is give")
        self.assertEqual(self.Card1.getSuit(), 3, "Correct suit is give")
        self.assertEqual(self.Card3.getSuit(), 4, "Correct suit is give")


if __name__ == '__main__':
    unittest.main()
