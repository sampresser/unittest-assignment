import unittest
from card import Card


class CardInitRaiseValueError(unittest.TestCase):
    def setUp(self):
        pass

    # Chee, this test will raise an exception if both values of suit and rank are empty string
    def test_empty_String_parameters(self):
        with self.assertRaises(TypeError) as typeException:
            self.Card1 = Card(' ', ' ')
        self.assertEqual(typeException, "Type error can't be empty string")

    # chee, this test will raise an exception if the value of both suit and rank are non
    def test_noneParameters(self):
        with self.assertRaises(NameError) as nameException:
            self.Card = Card(None, None)
        self.assertEqual(nameException, "Type can't be None")

    # Chee, this test will raise an exception if the suit value is none
    def test_noneSuit(self):
        with self.assertRaises(NameError) as nameException:
            self.Card = Card(None, 13)
        self.assertEqual(nameException, "suit can't be None")

    # Chee, this test will raise an exception if rank values are none
    def test_noneRank(self):
        with self.assertRaises(TypeError) as typeException:
            self.Card = Card(1, None)
        self.assertEqual(typeException, "Rank can't be None")

    # chee, this test will raise any exception if the suit and rank values are string type
    def test_string_suit_and_rank(self):
        with self.assertRaises(TypeError) as typeException:
            self.Card = Card("two", "three")
        self.assertEqual(typeException, "Values can't be string types")

    # Chee, this test will raise any exception if the suit value is a string
    def test_stringSuit(self):
        with self.assertRaises(TypeError) as typeException:
            self.Card = Card("three", 5)
        self.assertEqual(typeException, "suit can't be string type")

    # Chee, this test will raise an exception if the value of rank is a string type
    def test_stringRank(self):
        with self.assertRaises(TypeError) as typeException:
            self.Card = Card(1, "four")
        self.assertEqual(typeException, "Rank can't be String")

    # Chee, this test will raise an exception if the range of rank and suit are out of bound
    def test_outOfRangeValues(self):
        with self.assertRaises(ValueError) as valueException:
            self.Card = Card(-1, -2)
        self.assertEqual(valueException, "values is out of range")

    # Chee, this test will raise exceptions if any of the suit value is out range
    def test_suit_out_of_range(self):
        with self.assertRaises(ValueError) as valueException:
            self.Card = Card(-1, 3)
        self.assertEqual(valueException, "suit is out of range")

    # Chee, this test will raise any exceptions if the range are out of bounds
    def test_rankOutOfBound(self):
        with self.assertRaises(ValueError) as ValueException:
            self.Card = Card(2, 17)
        self.assertEqual(ValueException, "Rank is out of rank")


if __name__ == '__main__':
    unittest.main()
