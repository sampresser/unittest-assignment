import unittest
from card import Card


class CardInitCorrectRankAndSuit(unittest.TestCase):
    def setUp(self):
        self.Card = Card(2, 13)
        self.Card1 = Card(3, 1)
        self.Card3 = Card(4, 12)

    # chee, this test is testing for the constructor to see the correct rank and suit is being providing
    def test_cardInitCorrectRankAndSuit(self):
        self.Card4 = Card(1, 2)
        self.assertEqual(self.Card.__init__(4, 4), "initialized the correct rank and suit")
        self.assertEqual(self.Card3.__init__(3, 5), "Init the correct rank and suit")
        self.assertEqual(self.Card4.__init__(2, 11), "Init the correct ran and suit")

    # Chee, this test will test for the correct initialized values of rank and suit
    def test_cardInitInvalidRankAndSuit(self):
        self.Card4 = Card(3, 9)
        self.assertEqual(self.Card.__init__(-1, 15), "incorrect suit and rank init")
        self.assertEqual(self.Card3.__init__(0.3, 13.2), "incorrect suit and rank init")
        self.assertEqual(self.Card.__init__(0, -2), "incorrect suit and rank init")
        self.assertEqual(self.Card.__init__(5, 200), "incorrect suit and rank init")
        self.Card5 = Card(1, 5)
        self.assertEqual(self.Card5.__init__(' ', ' '), "suit and rank can't be empty")


if __name__ == '__main__':
    unittest.main()
