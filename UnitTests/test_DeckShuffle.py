import unittest
from deck import Deck


class DeckShuffle(unittest.TestCase):
    def setUp(self):
        self.Deck1 = Deck()
        self.Deck2 = Deck()

    def test_DeckShuffle(self):
        unshuffled = []
        shuffled = []
        for n in self.Deck1.draw():
            unshuffled.append(n)
        self.Deck1.shuffle()
        for n in self.Deck1.draw():
            shuffled.append(n)

        self.assertNotEqual(unshuffled, shuffled)

    def test_DeckShuffleCount(self):
        self.Deck2.shuffle()
        count = 0
        for n in self.Deck2.count():
            count = count + 1
        self.assertEquals(count, 52)


if __name__ == '__main__':
    unittest.main()