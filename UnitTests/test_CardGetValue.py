import unittest
from card import Card


class CardGetValueSuite(unittest.TestCase):
    def setUp(self):
        self.Card = Card(2, 13)
        self.Card1 = Card(3, 1)
        self.Card3 = Card(4, 12)

    # Chee, this test will test for any correct value for both suit and rank
    # 1. if both of them are out of bounds and the numbers don't give the correct max values
    def test_incorrectCardValue(self):
        self.assertEqual(self.Card.getValue(), 25, " this shouldn't pass")
        self.assertEqual(self.Card1.getValue(), 10, "This test should failed")
        self.assertEqual(self.Card3.getValue(), 0, "This test should failed")

    # Chee, this test will test for the correct value for both suit and rank
    # this test will output the correct vaules of a card
    def test_correctCardValue(self):
        self.assertEqual(self.Card.getValue(), 14, "This test should pass, with a value 14")
        self.assertEqual(self.Card1.getValue(), 4, "this test should pass, with a value 4")
        self.assertEqual(self.Card3.getValue(), 16, "This test should pass, with a value 16")

    # Chee, this test will test for both big and small values of rank and suits
    def test_incorrectValuesSmallAndBig(self):
        self.Card4 = Card(-1, -200)
        self.assertEqual(self.Card4.getValue(), -201, "Cards should not accept any negative values")
        self.Card5 = Card(0.1111111111, 0.2222222222)
        self.assertEqual(self.Card5.getValue(), 0.3333333333, "Cards should not return any values that are small")
        self.Card6 = Card(9000000000, 1000000000)
        self.assertEqual(self.Card6.getValue(), 1000000000, "Cards should not return any values that are too large")


if __name__ == '__main__':
    unittest.main()
