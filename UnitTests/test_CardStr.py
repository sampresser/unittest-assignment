import unittest
from card import Card


class CardStr(unittest.TestCase):
    def setUp(self):
        self.Card1 = Card(1, 3)
        self.Card2 = Card(2, 5)
        self.Card3 = Card(3, 6)

    # Chee, This is is to test if values of rank were
    #  1. out of bounds
    #  2. a None type
    #  3. a string types
    # if any of these conditions meet, then a message will be show
    def test_incorrect_rank_values(self):
        self.Card1.__init__(14, 14)
        self.assertEqual(self.Card1.__str__(), "Can't convert range that of suit/rank of 14s")
        self.Card2.__init__(1, -16)
        self.assertEqual(self.Card2.__str__(), "Can't Covert suit that is out of rank")
        self.Card3.__init__(None, None)
        self.assertEqual(self.Card3.__str__(), "Can't convert None types")
        self.Card4 = Card(2, None)
        self.assertEqual(self.Card4.__str__(), "Can't convert rank type none")
        self.Card4 = Card(1, "P")
        self.assertEqual(self.Card4.__str__(), "Can't convert rank type String")

    # Chee, This is is to test if values of suit were
    #  1. out of bounds
    #  2. a None type
    #  3. a string types
    # if any of these conditions meet, then a message will be show
    def test_incorrect_suit_values(self):
        self.Card1.__init__(15, 15)
        self.assertEqual(self.Card1.__str__(), "can't convert to str with range out of bounds")
        self.Card2.__init__(-14, 13)
        self.assertEqual(self.Card2.__str__(), "can't convert to str with range out of bounds")
        self.Card3.__init__("p", 13)
        self.assertEqual(self.Card3.__str__(), "can't convert to str with str suit")
        self.Card4.__init__(" ", 13)
        self.assertEqual(self.Card4.__str__(), "can't convert to str with empty suit string")

    # 1:clubs 2:spades 3:hearts 4:diamonds Chee,
    # this test will use the correct suits and rank values and then will
    # display to the players on what they got on their hand as a cards string
    def test_correct_values_toString(self):
        self.assertEqual(self.Card1.__str__(), "3C successfully converted")
        self.assertEqual(self.Card2.__str__(), "5S successfully converted")
        self.assertEqual(self.Card3.__str__(), "6H successfully converted")
        self.Card4.__init__(4, 13)
        self.assertEqual(self.Card4.__str__(), "KD successfully converted")
        self.Card4.__init__(2, 12)
        self.assertEqual(self.Card4.__str__(), "QS successfully converted")
        self.Card4.__init__(1, 11)
        self.assertEqual(self.Card4.__str__(), "JC successfully converted")
        self.Card4.__init__(3, 10)
        self.assertEqual(self.Card4.__str__(), "10H successfully converted")
        self.Card4.__init__(2, 13)
        self.assertEqual(self.Card4.__str__(), "KS successfully converted")


if __name__ == '__main__':
    unittest.main()
