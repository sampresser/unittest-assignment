import unittest
from deck import Deck


class DeckDrawValue(unittest.TestCase):
    def setup(self):
        self.d1 = Deck()
        self.d2 = Deck()
        self.d3 = None

    # Chee, This test will use make used of the global variable, at the end of this test. There shouldn't be any
    # cards left in the deck
    def test_DeckDrawAllCards(self):
        test = globals()

        for n in range(1, 53):
            test = 52 - n
            self.d1.draw()

        self.assertEqual(self.d1.count(), test, "test should only have 0 cards remaining")

    # Chee, This test will test too see the number of cards left in the deck, with just 2 cards being draw
    def test_DeckDrawCouplesCards(self):
        myDeck = 52
        self.d2.draw()
        var = --myDeck
        self.d2.draw()
        var2 = --var
        self.assertEqual(self.d2.count(), var2, "there are should be only be 50 cards left")

    # Chee, this test will test to see if the correct cards that are remaining in the deck is correct after 3 cards
    # were draw.
    def test_deckDrawFewCards(self):
        myDeck = 52
        self.d1.draw()
        myDeck = myDeck - 1
        self.d1.draw()
        myDeck = myDeck - 1
        self.d1.draw()
        myDeck = myDeck - 1
        self.assertEqual(self.d1.count(), myDeck, " there are should be only be 49 cards left")

    def test_DeckDrawValueNone(self):
        with self.assertRaises(TypeError):
            self.d3.draw()


if __name__ == '__main__':
    unittest.main()
