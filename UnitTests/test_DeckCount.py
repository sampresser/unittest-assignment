import unittest
from card import Card
from deck import Deck


class DeckCount(unittest.TestCase):
    def setUp(self):
        self.d1 = Deck()
        self.d2 = None
        self.d3 = Card

    def test_DeckCountFullCorrectType(self):
        x = range(52)
        count = 52
        for n in x:
            self.assertEqual(self.d1.count(), count, "{}".format(n))
            count = count - 1
            self.d1.draw()

    def test_DeckCountNone(self):
        with self.assertRaises(TypeError) as typeException:
            self.d2.count()
        self.assertEqual(typeException, "Object is not the correct type")

    def test_DeckCountCardTyp(self):
        with self.assertRaises(TypeError) as typeException:
            self.d2.count()
        self.assertEqual(typeException, "Object is not the correct type")


if __name__ == '__main__':
    unittest.main()
