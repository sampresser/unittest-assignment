import unittest
from card import Card
from deck import Deck

from deck import Deck


class DeckDrawException(unittest.TestCase):
    def setUp(self):
        self.d1 = Deck()
        self.d2 = None

    # Chee, fixed this portions
    # what this test does is that if there are no more cards in the deck and if the dealer keep drawing, then
    # an exception will be raise
    def test_DeckDraw(self):
        x = range(56)
        for n in x:
            with self.assertRaises(ValueError) as valueException:
                self.d1.draw()
            self.assertEqual(valueException, "Drawing when there is no more cards")

    # chee, fix this portions of the codes,
    # what this does is that if an object card was crated as a type none, then
    # the object methods are accessing the None object, there NameError exception will be raise
    def test_DeckDrawExNone(self):
        with self.assertRaises(NameError) as nameException:
            self.d2.draw()
        self.assertEqual(nameException, "Drawing when object type is none")


if __name__ == '__main__':
    unittest.main()
